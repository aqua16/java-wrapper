package com.adaptivewrapper.repository;

import com.adaptivewrapper.entity.Specification;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpecificationRepository extends ReactiveMongoRepository<Specification,String> {

}
