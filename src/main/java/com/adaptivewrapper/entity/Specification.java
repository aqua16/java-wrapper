package com.adaptivewrapper.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Document(collection = "specifications")
public class Specification {

    @Id
    private String assessmentId;

    @NonNull
    private String title;

    @NonNull
    private Integer questionLimit;

    private Float precision;

    @NonNull
    private String model;

    private Float initialAbility;
    private String itemSelector;
    private String abilityEstimator;

    @NonNull
    private String createdOn;

    @NonNull
    private String modifiedOn;

    @Transient
    @Getter(onMethod_={@JsonIgnore})
    private boolean isNew;
}
