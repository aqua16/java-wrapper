package com.adaptivewrapper.service;

import com.adaptivewrapper.dto.SpecificationDTO;
import com.adaptivewrapper.entity.Specification;
import com.adaptivewrapper.repository.SpecificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class SpecificationServiceImpl implements SpecificationService {

    @Autowired
    private SpecificationRepository specificationRepository;

    private String currDateAsString() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
    }

    @Value("${specification.default.initial.ability}")
    private Float defaultInitialAbility;

    @Value("${specification.default.item.selector}")
    private String defaultItemSelector;

    @Value("${specification.default.ability.estimator}")
    private String defaultAbilityEstimator;

    @Override
    public Mono<Specification> addSpecification(SpecificationDTO dto) {
        Specification specification = new Specification();
        specification.setAssessmentId(dto.getAssessmentId());
        specification.setTitle(dto.getTitle());
        specification.setQuestionLimit(dto.getQuestionLimit());
        specification.setPrecision(dto.getPrecision());
        specification.setModel(dto.getModel());
        specification.setInitialAbility(defaultInitialAbility);
        specification.setItemSelector(defaultItemSelector);
        specification.setAbilityEstimator(defaultAbilityEstimator);
        specification.setCreatedOn(currDateAsString());
        specification.setModifiedOn(currDateAsString());
        specification.setNew(true);

        return specificationRepository.save(specification);
    }

    @Override
    public Flux<Specification> getAllSpecifications() {
        return specificationRepository.findAll();
    }

    @Override
    public Mono<Specification> getSpecificationById(String id) {
        return specificationRepository.findById(id);
    }

    @Override
    public Mono<Specification> updateSpecificationById(String id, SpecificationDTO dto) {

        Mono<Specification> existingSpec = specificationRepository.findById(id);

        return existingSpec.flatMap(spec -> {
            spec.setAssessmentId(id);
            boolean isToUpdate = false;

            if (dto.getTitle() != null && !dto.getTitle().trim().isEmpty()
                    && !dto.getTitle().trim().equals(spec.getTitle().trim())) {
                spec.setTitle(dto.getTitle());
                isToUpdate = true;
            }

            if (dto.getQuestionLimit() != null && !dto.getQuestionLimit().equals(spec.getQuestionLimit())) {
                spec.setQuestionLimit(dto.getQuestionLimit());
                isToUpdate = true;
            }

            if (dto.getPrecision() != null && !dto.getPrecision().equals(spec.getPrecision())) {
                spec.setPrecision(dto.getPrecision());
                isToUpdate = true;
            }

            if (dto.getModel() != null && !dto.getModel().equals(spec.getModel())) {
                spec.setModel(dto.getModel());
                isToUpdate = true;
            }

            if (dto.getInitialAbility() != null && !dto.getInitialAbility().equals(spec.getInitialAbility())) {
                spec.setInitialAbility(dto.getInitialAbility());
                isToUpdate = true;
            }

            if (dto.getItemSelector() != null && !dto.getItemSelector().equals(spec.getItemSelector())) {
                spec.setItemSelector(dto.getItemSelector());
                isToUpdate = true;
            }

            if (dto.getAbilityEstimator() != null && !dto.getAbilityEstimator().equals(spec.getAbilityEstimator())) {
                spec.setAbilityEstimator(dto.getAbilityEstimator());
                isToUpdate = true;
            }

            spec.setNew(false);

            if(isToUpdate) {
                spec.setModifiedOn(currDateAsString());
                return specificationRepository.save(spec);
            }
            else return Mono.just(spec);
        });
    }

    @Override
    public Mono<Void> deleteSpecificationById(String id) {
        return specificationRepository.deleteById(id);
    }

}
